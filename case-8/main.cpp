/*
 * File:   main.cpp
 * Author: steve
 *
 * Created on 10 de noviembre de 2014, 12:51
 */

#include <cstdlib>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cassert>

using namespace std;

/*
 *
 */
int main(int argc, char** argv) {


    string command;
    double temperature=0;
    double humidity=0;
    double light=0;
    double ultra=0;
    double sound=0;
    double flow=0;
    double volume=0;
    double nitrogen=0;
    double carbon=0;
    double lat=0;
    double lon=0;
    srand(time(NULL));
    for(int i=0;i<100;i++){
        stringstream ss;
        temperature=(((int)(rand()*1000)/100))%300;
        if(temperature<0) temperature*=-1;
        temperature/=10;

        humidity=(((int)(rand()*1000)/100))%300;
        if(humidity<0) humidity*=-1;
        humidity/=10;

        light=(((int)(rand()*1000)/100))%300;
        if(light<0) light*=-1;
        light/=10;

        ultra=(((int)(rand()*1000)/100))%300;
        if(ultra<0) ultra*=-1;
        ultra/=10;

        sound=(((int)(rand()*1000)/100))%300;
        if(sound<0) sound*=-1;
        sound/=10;

        flow=(((int)(rand()*1000)/100))%300;
        if(flow<0) flow*=-1;
        flow/=10;

        volume=(((int)(rand()*1000)/100))%300;
        if(volume<0) volume*=-1;
        volume/=10;

        nitrogen=(((int)(rand()*1000)/100))%300;
        if(nitrogen<0) nitrogen*=-1;
        nitrogen/=10;

        carbon=(((int)(rand()*1000)/100))%300;
        if(carbon<0) carbon*=-1;
        carbon/=10;

        lat=(((int)(rand()*1000)/100))%90;
        if(lat<0) lat*=-1;
        lat/=10;

        lon=(((int)(rand()*1000)/100))%90;
        if(lon<0) lon*=-1;
        lon/=10;

        //cout<<temperature<<endl;
        ss<<"curl -X POST -i -H \"Content-type: application/json\"  -X POST https://sample-project-smart.herokuapp.com/api/update/ -d '{";
        ss<<"\"device\": \"166d77ac1b46a1ec38aa35ab7e628ab5\","
            <<"\"pub_date\": \"2014-07-15T22:02:27.321Z\","
            <<"\"temperature\": \""<<temperature<<"\","
            <<"\"humidity\": \""<<humidity<<"\","
            <<"\"light\": \""<<light<<"\","
            <<"\"ultra_violet\": \""<<ultra<<"\","
            <<"\"sound\": \""<<sound<<"\","
            <<"\"flowmeter\": \""<<flow<<"\","
            <<"\"volume\": \""<<volume<<"\","
            <<"\"nitrogen_dioxide\": \""<<nitrogen<<"\","
            <<"\"carbon_monoxide\": \""<<carbon<<"\","
            <<"\"latitude\": \""<<lat<<"\", "
            <<"\"longitude\": \""<<lon<<"\""
            <<"}'"<<endl;

        command=ss.str();
        //cout<<command<<endl;
        system(command.c_str());
        cout<<endl;
    }

    //cout<<command<<endl;
    return 0;
}
